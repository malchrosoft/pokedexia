<?php
	
error_reporting(E_ALL);
ini_set("display_errors", 1);

setlocale(LC_TIME, "fr_FR.utf8");


include_once './includes/header.php';


$ficheArrayFileName = "";
if (isset($_GET))
{
	$ficheArrayFileName = $_GET['fiche'];
}

$localFilePath = $affDirPath . "/" . $ficheArrayFileName;

$ficheJson = file_get_contents($localFilePath);
$ficheArray = json_decode($ficheJson);

$picturePathBase = $affDirPath . "/" . $ficheArray->picture;
$picturePath = $picturePathBase . ".jpg";
$picturePathPNG = $picturePathBase . ".png";

?>


<div class="navigation"><a class="link" href="./index.php?fiche=<?php echo $ficheArrayFileName; ?>" title="Retourner à la liste"><i class="fa fa-angle-double-left"></i> &nbsp;<i class="fa fa-th"></i>&nbsp;</a></div>
<div class="sub-title"><i title="" class="fa fa-id-card-o fa-2x"></i> Fiche d'identité</div>
<h1><?php echo $ficheArray->name; ?></h1>
<div class="big-picture" style="background-image: url(<?php 
	if (file_exists($picturePath)) 
	{
		echo $picturePath;
	}
	else if (file_exists($picturePathPNG))
	{
		echo $picturePathPNG;
	}
	else
	{
		echo $defaultPicturePath;
	}
	?>);">
	<div class='picture-sign'>PokédexIA©</div>
</div>
<div class="description" ><?php echo $ficheArray->description; ?></div>
<div class="fiche-details">
	<div class="fiche-panel">
		<div class="label">Taille</div>
		<div class="value"><?php echo str_replace(".", ",", "".$ficheArray->size) ?> m</div>	
	</div>
	<div class="fiche-panel">
		<div class="label">Poids</div>
		<div class="value"><?php echo str_replace(".", ",", "".$ficheArray->weight) ?> Kg</div>	
	</div>
	<div class="fiche-panel">
		<div class="label">Genre</div>
		<div class="value gender">
			<?php 
				foreach ($ficheArray->gender as $gend)
				{
					if ($gend === "G" || $gend === "M")
					{
						echo '<i class="fa fa-male fa-2x male"></i>&nbsp;';
					}
					if ($gend === "F")
					{
						echo '<i class="fa fa-female fa-2x female"></i>&nbsp;';
					} 
				}
				
			?>
		</div>
	</div>
</div>
<div>
	<?php 
		if ($ficheArray->evolution) 
		{
			$evolFileName = $affDirPath . "/" . $ficheArray->evolution . ".json";
			if (file_exists($evolFileName))
			{
				$ficheEvolJson = file_get_contents($evolFileName);
				$ficheEvol = json_decode($ficheEvolJson);
		?> 
	<span class="navigation">Evolution : <a class="link" href="./fiche.php?fiche=<?php echo $ficheArray->evolution . ".json"; ?>" title="Ouvrir la fiche"><?php echo $ficheEvol->name; ?>&nbsp;<i class="fa fa-angle-double-up"></i></a></span>
	<?php
			}
			else
			{
				?> 
	<span class="navigation">Evolution : <span class="link danger" title="Impossible de retrouver la fiche"><?php echo $ficheArray->evolution; ?>&nbsp;<i class="fa fa-angle-double-up"></i></span></span>
	<?php
				
			}
		}
		?>
</div>


<?php include_once './includes/footer.php'; ?>
