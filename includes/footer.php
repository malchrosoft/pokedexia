<?php
?>

		<!-- FOOTER START -->
           </div>
            <footer>
                <div class="left">
                    <h2>La Companie PokémonIA</h2>
                    <ul>
                        <li><a href="./histoire.php">Histoire</a></li>
                        <li><a href="#">Actualités</a></li>
                        <li><a href="#">Artistes</a></li>
                        <li><a href="#">Réalisation</a></li>
                    </ul>
                </div>
                <div class="right">
                    <p style="text-align: center;">
                        <br>
                        <i>"Laissez votre imagination se concrétiser !"</i><br>
                        <br>
                        © 2021&nbsp;&nbsp;<b><i class="fa fa-id-card-o fa-1x"></i> PokédexIA</b>
                        <br /> Tous
                        droits réservés - <a target="_blank" href="http://fr.wikipedia.org/wiki/Propri%C3%A9t%C3%A9_intellectuelle"
                            alt="Définition détaillée sur Wikipedia" title="Définition détaillée sur Wikipedia">Propriété
                            intellectuelle</a><br>
							<br>
							<a target="_blank" href="https://fontawesome.com/v4.7.0/icons"> Icons from <i class="fa fa-flag fa-1x"></i> FontAwesome 4.7 version</a>
							<br>
							<br>
                        Irina & Anakîn MALCHROWICZ
                    </p>
            
                </div>
            </footer>
        </div>
    </body>
</html>	