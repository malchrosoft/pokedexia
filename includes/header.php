<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>PokédexIA</title>
        <link rel="stylesheet" href="./includes/css/font-awesome.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <link rel="stylesheet" type="text/css" href="./includes/css/style.css" />

        <script type="text/javascript" src="./includes/js/jquery.js"></script>
    </head>
    <body>
        <header>
            <div class="site-title">PokédexIA
                <div class="site-subtitle">Imaginé par Irina et Anakîn</div>
            </div>
        </header>
        <div class="global-site">
            <div class="main-container">
				
				<!-- HEADER END -->
<?php
	// HEADER
	$affDirPath = "./pokemons";
	$defaultPicturePath = $affDirPath . "/unknown.jpg";
?>				
				

