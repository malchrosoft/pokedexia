
<?php
	
error_reporting(E_ALL);
	ini_set("display_errors", 1);

	setlocale(LC_TIME, "fr_FR.utf8");


	include_once './includes/header.php';
?>
<h1>Liste des pokémons</h1>


				<?php
					$files = scandir($affDirPath);
					$count = 0;
					foreach ($files as $file)
					{
						if (!strpos($file, ".json"))
						{
							continue;
						}
						$count++;
						
						$localImgPath = $affDirPath . "/" . $file;
						$ficheJson = file_get_contents($localImgPath);
						$ficheArray = json_decode($ficheJson);
//						var_dump($ficheArray->gender);
						
						$picturePathBase = $affDirPath . "/" . $ficheArray->picture;
						$picturePath = $picturePathBase . ".jpg";
						$picturePathPNG = $picturePathBase . ".png";
						
						?>
						<a class="mini-fiche" href="./fiche.php?fiche=<?php echo $file; ?>" title="Ouvrir la fiche">
							<div class="picture" style="background-image: url(<?php 
								
								if (file_exists($picturePath)) 
								{
									echo $picturePath;
								}
								else if (file_exists($picturePathPNG))
								{
									echo $picturePathPNG;
								}
								else
								{
									echo $defaultPicturePath;
								}
								?>);"></div>
							<div class="pok-num">N° <?php echo ($count < 10 ? "00" : ($count < 100 ? "0" : "")) . "$count"; ?></div>
							<div class="pok-name"><?php echo $ficheArray->name ?></div>
							<div class="pok-abilities">
								<?php 
									foreach ($ficheArray->abilities as $ability)
									{
										?><div class="pok-ability <?php echo $ability; ?>">
											<?php echo strtoupper(substr($ability, 0, 1)) . substr($ability, 1); ?></div><?php ;
									}
								?>
							</div>
						</a>
				
						<?php
							
					}
				?>
<div class="count">Nombre total de pokémons dans notre PokédexIA : <b><?php echo $count ?></b></div>

 <?php include_once './includes/footer.php'; ?>